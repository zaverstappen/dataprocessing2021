from os.path import join

workdir: "/homes/zaverstappen/Desktop/3e_jaar/Minor/dataprocessing2021/WC-05/"
DATA = '/commons/Themas/Thema11/Dataprocessing/WC02/data/'
configfile: "config.yaml"


rule bwa_map:
    input:
        join(DATA, config["genome"]+config["ext"]),
        join(DATA, "samples/{sample}.fastq")
    output:
        "mapped_reads/{sample}.bam"
    benchmark:
        "benchmarks/{sample}.bwa.benchmark.txt"
    log:
        "logs/bwa_mem/{sample}.log"
    threads: 8
    shell:
        "(bwa mem -t {threads} {input} | samtools view -Sb - > {output}) 2> {log}"


rule samtools_sort:
    input:
        "mapped_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam"
    log:
        "logs/sam_sort/{sample}.log"
    shell:
        "(samtools sort -T sorted_reads/{wildcards.sample} "
        "-O bam {input} > {output}) 2> {log}"


rule samtools_index:
    input:
        "sorted_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam.bai"
    log:
        "logs/sam_index/{sample}.log"
    shell:
        "(samtools index {input}) 2> {log}"


rule bcftools_call:
    input:
        fa="data/genome.fa",
        bam=expand("sorted_reads/{sample}.bam", sample=config["samples"]),
        bai=expand("sorted_reads/{sample}.bam.bai", sample=config["samples"])
    output:
        "calls/all.vcf"
    log:
        "logs/bcf_call/all.log"
    shell:
        "(samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}) 2> {log}"



rule report:
    input:
        T1="calls/all.vcf",
        T2=expand("benchmarks/{sample}.bwa.benchmark.txt", sample=config["samples"])
    output:
        "out.html"
    run:
        from snakemake.utils import report
        with open(input[0]) as f:
            n_calls = sum(1 for line in f if not line.startswith("#"))

        report("""
        An example workflow
        ===================================

        Reads were mapped to the Yeast
        reference genome and variants were called jointly with
        SAMtools/BCFtools.

        This resulted in {n_calls} variants (see Table T1_).
        Benchmark results for BWA can be found in the tables T2_.
        """, output[0], metadata="Author: Zoe Verstappen " ,**input)

rule clean:
    shell:
        'rm *.html'


