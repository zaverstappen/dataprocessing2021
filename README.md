# Repository of dataprocessing course  
__Author__: Zoë Verstappen  
__Date__: 22-06-2021  
__Course__: Dataprocessing(BFVH4DPS1) Thema 11 2021  

This repository contains the files with the results of the assignments that belong to this course. The directories WC-01 - WC-05 contain the results of the tutorials. The directory Final-assignment contains the corresponding files for main assignment. The Final assignment is explained below.  

## Workflow of snakemake pipeline
### Pipeline analyzing the effect of 4 treatments on 4 different brain regions in the mouse  

![Workflow](./Final-assignment/final_dag.png)

The workflow starts with downloading the index files of the *Mus musculus* (mm10) genome. The next step is mapping the reads of the 16 samples. The index of the genome and the forward and reverse reads of the sample are used by bowtie2. After the mapping step, the files are converted to bam files, the files are sorted and lastly indexed by the samtools. After these steps the coverage of the reads of the samples are calculated by the tool bamCoverage fromn deeptools. The bext step is creating a multiBigwig file for every brain region that contains the 4 coverage file of the that specific brain region. The last step of the workflow is creating a PCA for the different brain region using those multiBigwig files. the output are 4 pdf files each containing a PCA of a brain region. The visualization of the pipeline can also be viewed in the 'final_dag.png' in the Final-assignment folder.

### Information about the performed process
The pipeline of this project was runned on assemblix using 40 cores. All intermediate files are made temporary and are automatically deleted when they are no longer needed by the pipeline. 

### Components of Final-assignment directory  
- logs/ , this directory contains the log files of the performed shell commands of the pipeline.
- Project_details/ , this directory contain the information about the project and the old pipeline that was created.
- benchmarks/ , directory that contains the benchmarks of the jobs performed by snakemake.
- Results/ , directory that contains directory multiBigwigs/ that contains the multiBigwig summaries of the brain regions. (Results/ should contain 4 pfd files with pca's but these were not developed because of an error.)
- config.yaml, this file contains the data paths and several names used in the pipeline.
- main.Snakefile, main script with rule all that needs to be executed for running the pipeline.
- mapping.Snakefile, file contains rules for downloading the index genome and mapping the reads using bowtie2.
- samtools.Snakefile, file that contains the rules for coverting the file to bam files, sortting the files and indexing the files.
- coverage.Snakefile, this file contains the rule for calculating the coverage of the reads.
- PCA.Snakefile, this file contains the rules for creating a multiBigWig file and performing a PCA on the brain regions.


## Running the pipeline yourself
__*Warning! The data used for this project is only accessable on the bin netwerk of the Hanze hogeschool groningen.*__

### Required tools:
* [Python 3](https://www.python.org/downloads/release/python-373/)(version 3.7.3) + libraries:
    * os.path
    * glob
    * os
    * re 
* wget  
* Virtual environment
* [Snakemake](https://snakemake.readthedocs.io/en/v5.10.0/getting_started/installation.html) (version 5.10.0)
* [bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml) (version 2.3.4.3)
* [samtools](http://www.htslib.org/download/) (version 1.9)
* [deeptools](https://deeptools.readthedocs.io/en/develop/content/installation.html) (version 3.5.1)

### Cloning the project
Clone the project to be able to use the snakemake pipeline from:  
```
git clone https://zaverstappen@bitbucket.org/zaverstappen/dataprocessing2021.git
``` 
### Create an virtual environment
Create a virtual environment(if not present) in which the tools will be installed and the pipeline will be run.  
```
virtualenv -p /usr/bin/python3 venv
```
### Activate the virtual environment
```
source venv/bin/activate
```
### Install snakemake and the other used software if not present.
See references for help to install.

### Changing data paths
Change the data paths in config.yaml 

- working_dir, path to working directory.   
- results_dir, path of directory where the results need to be stored.    
- configfile, path to directory where to config file is stored.    

### Visualizing the results. 
Go into the folder of this project en go to the "Final-assignment" directory.
You can execute the pipeline by typing:
```
snakemake --snakefile main.Snakefile
```

Optionally you can set the number of cores that the pipeline will use. This will make the program run faster, some of the tools may take a while before finishing.
```
snakemake --snakefile main.Snakefile --cores {number of cores}
```  
  
  

