# -*- python -*-

"""
Snakemake script of the rule needed for calculating the coverage.
"""

#Rule for calculating coverage
rule bamCoverage:
    input:
        bam = 'sorted_reads/sorted_{sample}.bam',
        bai = 'sorted_reads/sorted_{sample}.bam.bai'
    output:
        temp('bam_coverage/coverage_{sample}.bw')
    message:
        'Calculating coverage of bam file: {wildcards.sample}'
    benchmark:
        'benchmarks/bam_coverage/{sample}.bamCoverage.benchmark.txt'
    log:
        join(results_dir, 'logs/bam_coverage/{sample}.bamCoverage.log')
    shell:
        '(bamCoverage -b {input.bam} -o {output} -of bigwig --normalizeUsing RPKM --binSize 10) 2> {log}'