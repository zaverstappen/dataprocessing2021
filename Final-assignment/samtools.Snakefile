# -*- python -*-

"""
Snakemake script of the rules needed for samtools.
"""
#Rule for converting sam to bam file
rule samtools_view:
    input:
        'mapped_reads/{sample}.sam'
    output:
        temp('mapped_reads/{sample}.bam')
    threads:
        10
    message:
        'Converting sam to bam file of sample: {wildcards.sample}'
    benchmark:
        'benchmarks/samtools/{sample}.samtools.sort.benchmark.txt'
    log:
        join(results_dir, 'logs/samtools/{sample}.samtools.sort.log')
    shell:
        '(samtools view --threads {threads} -bS {input} > {output}) 2> {log}'



#Rule for sorting mapped reads
rule samtools_sort:
    input:
        'mapped_reads/{sample}.bam'
    output:
        temp('sorted_reads/sorted_{sample}.bam')
    threads:
        10
    message:
        'Sorting mapped reads of sample: {wildcards.sample}'
    benchmark:
        'benchmarks/samtools/{sample}.samtools.sort.benchmark.txt'
    log:
        join(results_dir, 'logs/samtools/{sample}.samtools.sort.log')
    shell:
        '(samtools sort --threads {threads} {input} > {output}) 2> {log}'

#Rule for indexing
rule samtools_index:
    input:
        'sorted_reads/sorted_{sample}.bam'
    output:
        temp('sorted_reads/sorted_{sample}.bam.bai')
    threads:
        10
    message:
        'Indexing sorted reads of sample: {wildcards.sample}'
    benchmark:
        'benchmarks/samtools/{sample}.samtools.index.benchmark.txt'
    log:
        join(results_dir, 'logs/samtools/{sample}.samtools.index.log')
    shell:
        '(samtools index -@ {threads} {input}) 2> {log}'