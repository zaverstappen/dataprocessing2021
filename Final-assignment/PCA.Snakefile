# -*- python -*-

"""
Snakemake script of the rule needed for calculating the coverage.
"""

# Creating a multiBigwigSummary for each brain region
rule multiBigwigSummary:
    input:
        get_samples_of_brain_region
    output:
        'multiBigwigs/{brain_region}.npz'
    message:
        'Creating multiBigwigSummary for brain region: {wildcards.brain_region}.'
    benchmark:
        'benchmarks/multiBigwig/{brain_region}.benchmark.txt'
    log:
        join(results_dir, 'logs/multiBigwig/{brain_region}.log')
    shell:
        '(multiBigwigSummary bins --bwfiles {input} -o {output}) 2> {log}'

# Create PCA for each brain region
rule plot_PCA:
    input:
        'multiBigwigs/{brain_region}.npz'
    params:
        treatments = config['treatments'],
        title = 'PCA of brain region' +  '{wildcards.brain_region}'
    output:
        join(results_dir, 'Results/PCA_{brain_region}.pdf')
    message:
        'Creating PCA of multiBigwig of brain region: {wildcards.brain_region}'
    benchmark:
        'benchmarks/PCA/{brain_region}.benchmark.txt'
    log:
        join(results_dir,'logs/PCA/{brain_region}.log')
    shell:
        '(plotPCA -in {input} -o {output} -l {params.treatments} –T {params.title}) 2> {log}'