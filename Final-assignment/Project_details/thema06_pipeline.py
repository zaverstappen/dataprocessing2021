#!/usr/bin/env python3

"""
BFV2 Thema 06 - Insane in the Brain
"""

__author__ = "Gerlinde van Ginkel, Liesje Bloembergen, Eva Jakobs and Zoe Verstappen"
__status__ = "pipeline"
__version__ = "2019.v1"

import sys
import os
import argparse
import re

class Atac_pipeline:
    """
    class that is the pipeline of this project
    """
    def __init__(self, data):
        """
        constructor of class Atac_pipeline
        """
        self.data = data

    def fastqc(self, output_fastqc):
        """
        check the input file in fastqc
        :param output: string with output file name
        :return:
        """
        for file in os.listdir(self.data):
            os.system("fastqc {} -o {}".format(self.data, output_fastqc))

    def bowtie(self, cores):
        """

        :return:
        """
        list_r1 = list()
        list_r2 = list()

        for var in os.listdir(self.data):
            pattern = "(BE180925_\d\d\d_S\d{1,2}_R1_.+)"
            m = re.match(pattern, var)
            if m is None:
                pass
            else:
                file_string_R1 = m.group(1)
                list_r1.append(file_string_R1)
                file_string_R2 = file_string_R1.replace("_R1_", "_R2_")
                list_r2.append(file_string_R2)

        resultList = zip(list_r1, list_r2)
        file_dict = dict(resultList)
        for key,value in file_dict.items():
            os.system("bowtie2 -x {} -1 {} -2 {} -S {} -p {}".format("/data/storage/students/2018-2019/Thema06/"
                                                                     "index_mus_musculus/mm10", self.data + key, self.data + value, "alignment_data/" + key[9:12]
                                                                     + ".sam" , cores))

    def convert_sam_to_bam(self):
        for file in os.listdir("alignment_data"):
            os.system("samtools view -bS {} > {}".format(file, file.replace(".sam", ".bam")))

    def sort_bam(self):
        for file in os.listdir("alignment_data"):
            if file.endswith(".bam"):
                os.system("samtools sort {} > {}".format(file, "sorted_data/sorted_" + file))

    def index_bam(self):
        for file in os.listdir("sorted_data"):
            os.system("samtools index {}".format(file))

    def bam_coverage(self):
        for file in os.listdir("sorted_data"):
            if file.endswith(".bam"):
                os.system("bamCoverage -b {} -o {} -of bigwig --normalizeUsing RPKM --binSize 10".format(file, "bam_coverage/"+ file.replace(".bam", "coverage")))

    def multi_bigwig_summary(self):
        for sn in range(1,17,4):
            brain_region = ""
            sample_cortex = "001"
            sample_hippocampus = "005"
            sample_cerebellum = "009"
            sample_striatum = "013"

            bigwig_list = ["bam_coverage/sorted_{0:0>3}_coverage".format(sn), "bam_coverage/sorted_{0:0>3}_coverage".format(sn+1), "bam_coverage/sorted_{0:0>3}_coverage".format(sn+2),"bam_coverage/sorted_{0:0>3}_coverage".format(sn+3)]

            if sample_cortex in bigwig_list[0]:
                brain_region = "cortex"
            elif sample_hippocampus in bigwig_list[0]:
                brain_region = "hippocampus"
            elif sample_cerebellum in bigwig_list[0]:
                brain_region = "cerebellum"
            elif sample_striatum in bigwig_list[0]:
                brain_region = "striatum"
            else:
                print("sorry, no brainregion found")
            # klopt dit?? in de loop
            os.system("multiBigwigSummary bins –b {} -o {}.npz".format(*bigwig_list, brain_region))

    def plot_pca(self):
        treatment_list = ["P-P", "P-L", "L-P", "L-L"]
        for file in os.listdir("bam_coverage"):
            if file.endswith(".npz"):
                os.system("plotPCA -in {}-o {} -l {} –T {}".format(file, file.replace("npz", "pdf"), *treatment_list, file.strip(".npz")))


def main(args):
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", '--data directory', help="Fill in your directory with data", dest='data')
    parser.add_argument("-ofqc", '--output fastqc directory', help="Fill in your directory name for output fastqc", dest='output_fastqc')
    parser.add_argument("-p", '-- amount of cores', help="Fil in the amount of cores", dest='cores')
    args = parser.parse_args()
    ap = Atac_pipeline(args.data)
    ap.fastqc(args.output_fastqc)
    ap.bowtie(10)
    ap.convert_sam_to_bam()
    ap.sort_bam()
    ap.index_bam()
    ap.bam_coverage()
    ap.multi_bigwig_summary()
    ap.plot_pca()

if __name__ == "__main__":
    sys.exit(main(sys.argv))


