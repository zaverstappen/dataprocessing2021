# -*- python -*-

"""
Snakemake main script of the pipeline for analysing the effect
 of 4 treatments on the 4 main brain regions in the mouse.
"""

from os.path import join
from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider
import glob
import os
import re

# Creating a HTTP Provider for downloading the genome online
HTTP = HTTPRemoteProvider()

# Config file
configfile: '/homes/zaverstappen/Desktop/3e_jaar/Minor/dataprocessing2021/Final-assignment/config.yaml'

# Working directory
workdir: config['working_dir']

# Data directory
DATA = config['data_dir']

# Results directory
results_dir = config['results_dir']

# Fetch samples with glob.glob and remove common part
sample_names = [os.path.basename(x) for x in glob.glob(DATA + '*')]
SAMPLES = []
for sample in sample_names:
    pattern = "(BE180925_\d\d\d_S\d{1,2})"
    m = re.match(pattern, sample)
    if m is None:
        pass
    else:
        if m.group(0) not in SAMPLES:
            SAMPLES.append(m.group(0))
print(SAMPLES)

# Create dictionary asing wildcards as keys and the associated samples as value for each brain region
def create_sample_groups():
    sample_dict = {}
    samples = ['bam_coverage/coverage_' + s +'.bw' for s in SAMPLES]
    nums = [4,8,12,16]
    for sample, num, brain_region in zip(range(0,len(samples),4), nums, config['brain_regions']):
        sample_dict[brain_region] = list(samples[sample:num])
    return sample_dict

# Return associated samples(value) using the wildcards for the brain region as key
def get_samples_of_brain_region(wildcards):
    return create_sample_groups().get(wildcards.brain_region)

# Included files
include: 'mapping.Snakefile'
include: 'samtools.Snakefile'
include: 'coverage.Snakefile'
include: 'PCA.Snakefile'

# Last rule to check if the final results are present
rule all:
    input:
        expand(join(results_dir,'Results/PCA_{brain_region}.pdf'), brain_region=config['brain_regions'])
