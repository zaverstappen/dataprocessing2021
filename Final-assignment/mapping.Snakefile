# -*- python -*-

"""
Snakemake script of the rules needed for mapping.
"""

#Rule for downloading and unzipping reference genome
rule download_genome:
    input:
        HTTP.remote('https://genome-idx.s3.amazonaws.com/bt/mm10.zip')
    output:
        'mm10.1.bt2',
        'mm10.2.bt2',
        'mm10.3.bt2',
        'mm10.4.bt2',
        'mm10.rev.1.bt2',
        'mm10.rev.2.bt2',
        'make_mm10.sh'
    benchmark:
        'benchmarks/genome.benchmark.txt'
    threads:
        10
    message:
        'Downloading and unzipping reference genome "mm10".'
    run:
        shell('wget -nc {input}'),
        shell('unzip mm10.zip'),
        shell('rm mm10.zip')

# Rule for mapping the reads using bowtie2
rule bowtie2:
    input:
        'mm10.1.bt2',
        'mm10.2.bt2',
        'mm10.3.bt2',
        'mm10.4.bt2',
        'mm10.rev.1.bt2',
        'mm10.rev.2.bt2',
        'make_mm10.sh',
        r1 = join(DATA, ('{sample}' + '_R1' + config['ext'])),
        r2 = join(DATA, ('{sample}' + '_R2' + config['ext']))
    params:
        genome = config["genome"]
    output:
        temp('mapped_reads/{sample}.sam')
    threads:
        10
    message:
        'Mapping reads of sample: {wildcards.sample}'
    benchmark:
        'benchmarks/bowtie2/{sample}.bowtie2.benchmark.txt'
    log:
        join(results_dir, 'logs/bowtie2/{sample}.bowtie2.log')
    shell:
        '(bowtie2 --threads {threads} -x {params.genome} -1 {input.r1} -2 {input.r2} -S {output}) 2> {log}'